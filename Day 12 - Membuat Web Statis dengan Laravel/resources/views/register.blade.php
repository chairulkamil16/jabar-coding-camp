<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label for="firstName">First name:</label>
        <br><br>
        <input type="text" name="firstName">
        <br><br>

        <label for="lastName">Last name:</label>
        <br><br>
        <input type="text" name="lastName">
        <br><br>

        <label for="gender">Gender:</label>
        <br><br>
        <input type="radio" name="gender" value="male">Male
        <br>
        <input type="radio" name="gender" value="female">Female
        <br>
        <input type="radio" name="gender" value="other">Other
        <br><br>

        <label for="nasionality">Nasionality:</label>
        <br><br>
        <select name="nasionality" >
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="singaporean">Singaporean</option>
            <option value="australian">Australian</option>
        </select>
        <br><br>

        <label for="language">Language Spoken</label>
        <br><br>
        <input type="checkbox" name="language" value="bahasa indonesia">Bahasa Indonesian
        <br>
        <input type="checkbox" name="language" value="english">English
        <br>
        <input type="checkbox" name="language" value="other">Other
        <br><br>

        <label for="">Bio:</label>
        <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br>

        <input type="submit" value="submit">
    </form>
</body>
</html>