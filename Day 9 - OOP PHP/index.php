<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal('sahun');

$sungokong = new Ape('Kera sakti');

$buduk = new Frog('buduk');


?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Chairul Kamil</title>
</head>

<body>
  <table>
    <tr>
      <td>Name</td>
      <td>:</td>
      <td><?= $sheep->name ?></td>
    </tr>
    <tr>
      <td>Legs</td>
      <td>:</td>
      <td><?= $sheep->legs ?></td>
    </tr>
    <tr>
      <td>Cold Blooded</td>
      <td>:</td>
      <td><?= $sheep->cold_blooded ?></td>
    </tr>
    <tr>
      <td>Name</td>
      <td>:</td>
      <td><?= $buduk->name ?></td>
    </tr>
    <tr>
      <td>Legs</td>
      <td>:</td>
      <td><?= $buduk->legs ?></td>
    </tr>
    <tr>
      <td>Cold Blooded</td>
      <td>:</td>
      <td><?= $buduk->cold_blooded ?></td>
    </tr>
    <tr>
      <td>Jump</td>
      <td>:</td>
      <td><?= $buduk->jump() ?></td>
    </tr>
    <tr>
      <td>Name</td>
      <td>:</td>
      <td><?= $sungokong->name ?></td>
    </tr>
    <tr>
      <td>Legs</td>
      <td>:</td>
      <td><?= $sungokong->legs ?></td>
    </tr>
    <tr>
      <td>Cold Blooded</td>
      <td>:</td>
      <td><?= $sungokong->cold_blooded ?></td>
    </tr>
    <tr>
      <td>yell</td>
      <td>:</td>
      <td><?= $sungokong->yell() ?></td>
    </tr>

  </table>
</body>

</html>